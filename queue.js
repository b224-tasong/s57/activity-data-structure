let collection = [];

// Write the queue functions below.

module.exports = {

	print() {
		 return collection;
	},
	enqueue(data) {
	    collection.push(data);
	    return collection
	},
	dequeue() {
	    collection.shift();
	    return collection
	},
	front() {
		return collection[0];
	},
	size() {
		return collection.length;
	},
	isEmpty() {
		return collection.length === 0;
	}

};

